# This file is part of Adiczion's Tryton Module.
# The COPYRIGHT and LICENSE files at the top level of this repository
# contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import move


def register():
    Pool.register(
        move.PartyPayableReceivable,
        module='account_payable_receivable_extended', type_='report')

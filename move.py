# This file is part of Adiczion's Tryton Module.
# The COPYRIGHT and LICENSE files at the top level of this repository
# contains the full copyright notices and license terms.
import logging
from trytond.report import Report
from trytond.transaction import Transaction

logger = logging.getLogger(__name__)


class PartyPayableReceivable(Report):
    'Party Payable Receivable'
    __name__ = 'account.move.line.party_payable_receivable'

    @classmethod
    def get_context(cls, records, data):

        context = Transaction().context
        print('-*- context', context)
        report_context = super(PartyPayableReceivable, cls).get_context(
            records, data)
        print('-*- data', data)

        report_context['lines'] = records
        print('-*- records', records)
        return report_context
